Maser thesis LaTeX base
=======================

This base setup should work both with manual editing in your favourite text editor
as well as in [TeXstudio](http://texstudio.sourceforge.net), a simple LaTeX IDE.

Directory structure:

thesis/
 : include
      : Location of any special files like custom packages.

 : images
      : Location of any images you want to include in your thesis

 : build
       : The build output directory.

dictionaries
  : Dictionaries for Texstudio

LaTeX packages needed (Ubuntu):

  * texlive
  * texlive-latex-extra
  * texlive-lang-european (Suport for norwegian characters)

TeXstudio
-------------

The file *texstudio.txsprofile* contains the settings configured to work with the project structure.
Import the settings in Texstudio from "Options->Load Profile" or copy the file to `~/.config/texstudio/texstudio.ini`
(before running TeXstudio).

When opening TeXstudio for the first time, navigate to tex files and open main.tex.
Also, set the option "Options->Automatically Restore Session at Next Start" to have the project loaded automatically when running Texstudio.

To build and preview the result, press F1.

To add norwegian and english dictionaries:

 1. "Options->Configure Texstudio"
 2. In the General Tab, set the "Spelling Dictionary Directory" to the dictionaries directory of this project.
